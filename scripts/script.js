window.addEventListener("load", function () {
    const student = {
        _name: null,
        _lastName: null,
        tabel: {},
        get name() {
            return this._name;
        },
        get lastName() {
            return this._lastName;
        },
        setName: function (newName) {
            this._name = newName;
        },
        setLastName: function (newLastName) {
            this._lastName = newLastName;
        }
    };

    function validUserStringInput(inputValue) {
        return !(!inputValue || inputValue.trim() === "" || !isNaN(+inputValue));
    }

    function validUserNumberInput(inputValue) {
        return !(!inputValue || inputValue.trim() === "" || isNaN(+inputValue));
    }

    function setStudentTabel(obj, subject, score) {
        return obj.tabel[subject] = score;

    }

    function isStudentEnrolled(obj) {
        for (const key in obj) {
            if (typeof obj[key] === "function") {
                continue;
            }
            if (typeof obj[key] === "object" && obj[key] !== null) {
                for (const subject in obj[key]) {
                    if (obj[key][subject] < 4) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    function studentAverageScore(obj) {
        let scoreSumm = 0, subjectsCount = 0;
        for (const key in obj) {
            if (typeof obj[key] === "function") {
                continue;
            }
            if (typeof obj[key] === "object" && obj[key] !== null) {
                for (const subject in obj[key]) {
                    ++subjectsCount;
                    scoreSumm += obj[key][subject];
                }
            }
        }
        return scoreSumm / subjectsCount
    }

    let studentName, studentLastName, subjectName, studentScore;

    do {
        studentName = prompt("Please enter student's Name");
    }
    while (!validUserStringInput(studentName))

    do {
        studentLastName = prompt("Please enter student's last name");
    }
    while (!validUserStringInput(studentLastName))

    student.setName(studentName);
    student.setLastName(studentLastName);

    while (true) {
        do {
            subjectName = prompt("Enter student subject to set the score");
        }
        while (!validUserStringInput(subjectName))
        do {
            studentScore = prompt(`Enter student's score for ${subjectName}`);
        }
        while (!validUserNumberInput(studentScore))

        setStudentTabel(student, subjectName, +studentScore)

        if (!confirm("Would you like to set the score for another subject?")) {
            break;
        }
    }


    if (isStudentEnrolled(student)) {
        console.log("Student is enrolled to the next course");
    }
    if (studentAverageScore(student) > 7) {
        console.log("Student is set to receive scholarship");
    }
})

